#!/usr/bin/env bash
SETUP_PATH=$(PWD)
source ./beanworks.include.sh

cd $DCM_DIR/srv/bean
[ -d ui ] || git clone git@bitbucket.org:beanworks/beanworksui.git ui
cd ui 

cd classic && make dev-c9 
cd ../espresso && \
nohup make dev-hot-c9 &

# This will also starts a live webpack server to serve js and css on port 4443,
# so when you are done with it , please `kill $(pgrep webpack)` to kill the 
# local webpack server.
